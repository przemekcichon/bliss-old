<?php

class Bliss_Assets {
    
    public static function add( $type = 'styles', array $styles = null ){
        $style = new Bliss_Styles();
        
        return $style->enqueue( $styles );
        
    }
    
    public static function show(){
        $style = new Bliss_Styles();
        
        $string = '[';

        foreach( array_keys( $style->get_styles() ) as $handle ) {
            $string .= "'" .  $handle . "',";   
        }
        $string .= ']';
        echo $string;
    }

}
