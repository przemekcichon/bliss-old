<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-bliss-factory
 *
 * @author Przemek
 */
class Bliss_Factory {
    public $data;
    public $dir;
    public $plugin;
    
    public function __construct( $file, $data, $dir, $plugin ) 
    {
        $this->dir = $dir;
        $this->plugin = $plugin;
        $this->gatherData( $data );
        $this->template( $file );
    }
    
    public function template( $file )
    {      
        if( strpos( $file, '-' ) || strpos( $file, '.' ) ) :
            $pieces = strpos( $file, '-' ) ? explode( '-', $file, 2 ) : explode( '.', $file, 2 );
            $this->get_template_part( $this->dir . $pieces[0], $pieces[1] ); 
        else :
            $this->get_template_part( $this->dir . $file ); 
        endif;
        return $this;
    }
    
    public function get_template_part( $slug, $name = NULL )
    {
        $templates = [];
        $name = ( string ) $name;
        if( '' !== $name )
                $templates[] = "{$slug}-{$name}.php";
                
        $templates[] = "{$slug}.php";
        
        if(NULL !== $this->data ) extract ($this->data );
        include( $this->locate_template($templates, false, false) );   
    }
    
    public function locate_template($template_names, $load = false, $require_once = true )
    {
        if ( !is_array($template_names) )
            return '';

        $located = '';

        $this_plugin_dir = WP_PLUGIN_DIR.'/' . $this->plugin . '/public/';

        foreach ( $template_names as $template_name ) {
            if ( !$template_name )
                continue;
            if ( file_exists(STYLESHEETPATH . '/' . $template_name)) {
                $located = STYLESHEETPATH . '/' . $template_name;
                break;
            } else if ( file_exists(TEMPLATEPATH . '/' . $template_name) ) {
                $located = TEMPLATEPATH . '/' . $template_name;
                break;
            } else if ( $this->plugin !== 'none' && file_exists( $this_plugin_dir .  $template_name) ) {
                $located =  $this_plugin_dir . $template_name;
                break;
            }
        }

        if ( $load && '' != $located )
            load_template( $located, $require_once );

        return $located;
    }

    public function gatherData( $data )
    {
        if( NULL !== $data ) :
            foreach( $data as $key => $value ) :
                $this->data[$key] = $value;
            endforeach;         
        endif;    
    }    
}
