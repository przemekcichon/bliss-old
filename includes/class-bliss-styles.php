<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-bliss-styles
 *
 * @author Przemek
 */
class Bliss_Styles {
    
    public function register() {
        /* Get framework styles. */
        $styles = $this->get_styles();

        /* Loop through each style and register it. */
        foreach ( $styles as $style => $args ) {
            
            $defaults = array( 
                'handle'  => $style, 
                'src'     => $style == 'child-theme-style' ? trailingslashit( get_stylesheet_directory_uri() ) . "style.css" : trailingslashit( get_stylesheet_directory_uri() ) . "assets/css/{$style}.css",
                'deps'    => null,
                'version' => false,
                'media'   => 'all'
            );
                
            $args = wp_parse_args( $args, $defaults );
            
            wp_register_style(
                sanitize_key( $args['handle'] ), 
                esc_url( $args['src'] ), 
                is_array( $args['deps'] ) ? $args['deps'] : null, 
                preg_replace( '/[^a-z0-9_\-.]/', '', strtolower( $args['version'] ) ), 
                esc_attr( $args['media'] )
            );
            
        }
    }
    
    public function get_styles(){
        
        $child_theme_version = date( 'YmdHis', filemtime(trailingslashit( get_stylesheet_directory() ) . 'style.css') );
        $styles = [];
        foreach ( glob( get_stylesheet_directory() . "/assets/css/*.css") as $style) {
            $style = pathinfo($style); 
            if(!strpos($style['filename'], '.min')) {
               $styles[$style['filename']] = ['version' => $child_theme_version ]; 
            }    
        }

        $styles['child-theme-style'] = ['version' => $child_theme_version ];
        return $styles;
    }
    
    public function enqueue( array $styles = null ) {
        
        if( $styles == null ) {
            $styles = $this->get_styles();
        }
        $styles_handles = array_keys( $styles );

        foreach($styles_handles as $style ) {
            wp_enqueue_style(str_replace('.', '-', $style));
        } 
    } 
}
