<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Outputs an HTML element's attributes.
 *
 * @since  2.0.0
 * @access public
 * @param  string  $slug     The slug/ID of the element (e.g., 'sidebar').
 * @param  string  $context  A specific context (e.g., 'primary').
 * @return void
 */
function bliss_attr( $slug, $defaults = '', $context = '' ) {
	echo bliss_get_attr( $slug, $defaults, $context );
}
/**
 * Gets an HTML element's attributes.  This function is actually meant to be filtered by theme authors, plugins, 
 * or advanced child theme users.  The purpose is to allow folks to modify, remove, or add any attributes they 
 * want without having to edit every template file in the theme.  So, one could support microformats instead 
 * of microdata, if desired.
 *
 * @since  2.0.0
 * @access public
 * @param  string  $slug     The slug/ID of the element (e.g., 'sidebar').
 * @param  string  $context  A specific context (e.g., 'primary').
 * @return string
 */
function bliss_get_attr( $slug, $defaults = '', $context = '' ) {
	$out    = '';
	$attr   = apply_filters( "bliss_attr_{$slug}", array(), $context );
	if ( empty( $attr ) && empty( $defaults ) ) :
            $attr['class'] = $slug;
        elseif( empty( $attr ) ) :
            foreach ( $defaults as $name => $value ) :
                $out .= !empty( $value ) ? sprintf( ' %s="%s"', esc_html( $name ), esc_attr( $value ) ) : esc_html( " {$name}" );
            endforeach;
            return trim( $out );
        else :
            foreach ( $attr as $name => $value ) :
                $out .= !empty( $value ) ? sprintf( ' %s="%s"', esc_html( $name ), esc_attr( $value ) ) : esc_html( " {$name}" );
            endforeach;
            return trim( $out );
        endif;
}
