<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-bliss-attributes
 *
 * @author Przemek
 */
class Bliss_Attributes {
    public function __construct( $attributes ) 
    {         
        $this->attr( $attributes );
    }
    
    public function attr( $attributes ) 
    {
        $i = 0;
        while( current( $attributes ) ){
            $tag = 'bliss_attr_' . array_keys( $attributes )[$i];
            add_filter( $tag, function( $attr ) use ( $i, $attributes ) {
                $attr = current( $attributes );
                return $attr;
            });
            next( $attributes );
            $i++;	
        }     
        return $this;
    }
    
    public function render( $partial, $data = NULL, $plugin = 'none' )
    {
        return new Bliss_Factory( $partial, $data, 'partials/', $plugin );
    }
    
    public function create( $template, $data = NULL, $plugin = 'none'  )
    {
        return new Bliss_Factory( $template, $data, 'templates/', $plugin );
    }
    
    public function view( $template, $data = NULL, $plugin = 'none'  )
    {
        return new Bliss_Factory( $template, $data, 'tags/', $plugin );
    }
}
