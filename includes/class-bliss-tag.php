<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-bliss-tag
 *
 * @author Przemek
 */
class Bliss_Tag {
    public static function view( $template, $data = NULL, $plugin = 'none' ) 
    { 
        return new Bliss_Factory( $template, $data, 'tags/', $plugin );
    }
    
    public static function attr( $attributes )
    {
        return new Bliss_Attributes( $attributes );
    }
}