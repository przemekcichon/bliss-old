<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-bliss-partial
 *
 * @author Przemek
 */
class Bliss_Partial {
    public static function render( $partial, $data = NULL, $plugin = 'none' ) 
    { 
        return new Bliss_Factory( $partial, $data, 'partials/', $plugin );
    }
    
    public static function attr( $attributes )
    {
        return new Bliss_Attributes( $attributes );
    }
    
}
