<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.sajdnota.pl
 * @since      1.0.0
 *
 * @package    Bliss
 * @subpackage Bliss/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bliss
 * @subpackage Bliss/includes
 * @author     Przemek Cichon <przemekcichon@gmail.com>
 */
class Bliss_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
