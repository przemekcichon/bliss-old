<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.sajdnota.pl
 * @since      1.0.0
 *
 * @package    Bliss
 * @subpackage Bliss/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bliss
 * @subpackage Bliss/includes
 * @author     Przemek Cichon <przemekcichon@gmail.com>
 */
class Bliss_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
